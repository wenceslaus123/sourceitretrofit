package com.sourceit.sourceitretrofit;


import java.util.Map;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by wenceslaus on 11.11.16.
 */

public class RequestService {

    static WeatherService weatherService;

    public interface WeatherService {
        @GET("weather?")
        Observable<Map> getWeatherData(@Query("q") String city, @Query("APPID") String appId);
    }

    static {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();


        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://api.openweathermap.org/data/2.5/")
                .client(client)
                .build();

        weatherService = retrofit.create(WeatherService.class);
    }

    public static Observable<Map> getData(){
        return weatherService.getWeatherData("kharkiv", "fed403fe4bf340fa270d9a48cfa0b2cf");
    }

}
